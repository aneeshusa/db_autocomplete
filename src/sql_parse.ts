'use strict';

import { stripSuffix } from './util';


// If a value exists (is not `undefined`),
// then return it with a period,
// otherwise return the empty string.
function optPrefix(val: string): string {
    return (val !== undefined) ? `${val}.` : ``;
}


export const enum ParserState {
    New,
    Select,
    From,
    Where_first,
    Where_op,
    Where_second,
    On_first,
    On_op,
    On_second,
    Any,
    Unsupported,
};


export class Table {
    public schema: string;
    public table: string;

    constructor(tableSpec: string, tableName?: string) {
        if (tableName !== undefined) {
            // Schema and table passed explicitly
            this.schema = tableSpec;
            this.table = tableName;
        } else {
            [this.table, this.schema] = tableSpec.split('.').reverse();
        }
    }

    public toString() {
        return `${optPrefix(this.schema)}${this.table}`;
    }
}


export function table_equals(t1: Table, t2: Table) {
    // For some reason testing values gives undefined,
    // even though .toString() works...
    return (t1.toString() === t2.toString());
}


export class Field {
    public schema: string;
    public table: string;
    public attribute: string;

    constructor(fieldSpec: string, tableName?: string, attributeName?: string) {
        if (tableName !== undefined && attributeName !== undefined) {
            // Members were all passed explicitly
            this.schema = fieldSpec;
            this.table = tableName;
            this.attribute = attributeName;
        } else {
            [this.attribute, this.table, this.schema] = fieldSpec.split('.').reverse();
        }
    }

    public toTable() {
        return new Table(this.schema, this.table);
    }

    public toString() {
        return `${optPrefix(this.schema)}${optPrefix(this.table)}${this.attribute}`;
    }
}


export function field_equals(f1: Field, f2: Field) {
    // For some reason testing values gives undefined,
    // even though .toString() works...
    return (f1.toString() === f2.toString());
}


export class Condition {
    public first: Field;
    public op: string;
    public second: string;

    public toString() {
        return `${this.first} ${this.op} ${this.second}`;
    }
}

export class JoinCondition {
    public first: Field;
    public op: string;
    public second: Field;

    public toString() {
        return `${this.first} ${this.op} ${this.second}`;
    }
}


export class SqlQuery {
    public select: string[];
    public from: Table[];
    // TODO: handle joins
    public where: Condition[];
    public on: JoinCondition[];

    constructor() {
        this.select = [];
        this.from = [];
        this.where = [];
        this.on = [];
    }

    public reset() {
        this.select.length = 0;
        this.from.length = 0;
        this.where.length = 0;
        this.on.length = 0;
    }

    public toString(extra?: string) {
        const sources = this.select.join(', ');
        const tables = this.from.join(', ');
        const conditions = this.where.map((clause, idx) => {
            // TOOD: handle OR?
            const preposition = (idx === 0) ? "WHERE" : "AND";
            return `${preposition} ${clause}`
        }).join(', ');
        extra = (extra !== undefined) ? ` ${extra}` : ``;

        return `SELECT ${sources} FROM ${tables} ${conditions}${extra};`
    }

    public samplingQuery(field: Field): string {
        const attribute = field.toString();
        let clone = new SqlQuery(); // Note: will be shallow!
        clone.select = [attribute, `COUNT(${attribute})`];
        clone.from = this.from;
        clone.where = this.where;
        return clone.toString(
            `GROUP BY ${attribute} ORDER BY COUNT(${attribute}) DESC LIMIT 100`
        );
    };
}


export class LooseSqlParser {
    public state: ParserState;
    public query: SqlQuery;
    public currentWhere: Condition;
    public currentOn: JoinCondition;

    constructor() {
        this.state = ParserState.New;
        this.query = new SqlQuery();
        this.currentWhere = null;
        this.currentOn = null;
    }

    public reset() {
        this.state = ParserState.New;
        this.query.reset();
        this.currentWhere = null;
        this.currentOn = null;
    }

    // Loosely parse a subset of SQL,
    // which can handle missing clauses by skipping them,
    // and which won't return errors but will just
    // do its best to parse a partial query to
    // enable easier table exploration.
    public feedTokens(tokens) {
        tokens.forEach((token) => {
            token = stripSuffix(';', token);
            const tokenUpper = token.toUpperCase();
            switch(tokenUpper) {
                case "SELECT":
                case "DELETE":
                    this.state = ParserState.Select;
                    break;
                case "FROM":
                case "JOIN":
                    this.state = ParserState.From;
                    break;
                case "WHERE":
                case "AND":
                case "OR":
                    this.state = ParserState.Where_first;
                    this.currentWhere = new Condition();
                    break;
                case "ON":
                    this.state = ParserState.On_first;
                    this.currentOn = new JoinCondition();
                    break;
                case "GROUP":
                case "ORDER":
                    this.state = ParserState.Unsupported;
                    break;
                default:
                    switch(this.state) {
                        case ParserState.New:
                            break;
                        case ParserState.Select:
                            let fields = token
                                .split(',')
                                .filter((t) => t != '');
                            this.query.select.push(...fields);
                            break;
                        case ParserState.From:
                            // TODO handle aliases, e.g. table_name AS alias
                            let tables = token
                                .split(',')
                                .filter((f) => f != '')
                                .map((t) => new Table(t))
                                ;
                            this.query.from.push(...tables);
                            break;
                        case ParserState.Where_first:
                            // TODO: don't always assume first field is Field and second is constant
                            this.currentWhere.first = new Field(token);
                            this.state = ParserState.Where_op;
                            break;
                        case ParserState.Where_op:
                            this.currentWhere.op = token;
                            this.state = ParserState.Where_second;
                            break;
                        case ParserState.Where_second:
                            this.currentWhere.second = token;
                            this.query.where.push(this.currentWhere);
                            this.currentWhere = null;
                            this.state = ParserState.Any;
                            break;
                        case ParserState.On_first:
                            this.currentOn.first = new Field(token);
                            this.state = ParserState.On_op;
                            break;
                        case ParserState.On_op:
                            this.currentOn.op = token;
                            this.state = ParserState.On_second;
                            break;
                        case ParserState.On_second:
                            this.currentOn.second = new Field(token);
                            this.state = ParserState.Any;
                            this.query.on.push(this.currentOn);
                            this.currentOn = null;
                            break;
                        case ParserState.Any:
                            break;
                        case ParserState.Unsupported:
                            break;
                        default:
                            break;
                    }
                    break;
            }
        });
    }
}
