'use strict';

import * as vscode from 'vscode';

import { Field, field_equals, Table, table_equals, ParserState, LooseSqlParser } from './sql_parse';
import { flatten, includes, merge, parsePgSeq, intToSortText } from './util';
import { Stats } from './logger';


const COMPARATORS = [ "=", "<>", "!=", ">", "<", ">=", "<=", ];


const enum SqlType {
    Numeric,
    Temporal,
    String,
    Other,
}


function toSqlType(data_type: string): SqlType {
    const TYPE_MAPPING = {
        'smallint': SqlType.Numeric,
        'integer': SqlType.Numeric,
        'bigint': SqlType.Numeric,
        'decimal': SqlType.Numeric,
        'numeric': SqlType.Numeric,
        'real': SqlType.Numeric,
        'double precision': SqlType.Numeric,
        'smallserial': SqlType.Numeric,
        'serial': SqlType.Numeric,
        'bigserial': SqlType.Numeric,
        'timestamp': SqlType.Temporal,
        'timestamp without time zone': SqlType.Temporal,
        'timestamp with time zone': SqlType.Temporal,
        'date': SqlType.Temporal,
        'time': SqlType.Temporal,
        'time without time zone': SqlType.Temporal,
        'time with time zone': SqlType.Temporal,
        'character varying': SqlType.String,
        'text': SqlType.String,
    }
    if (data_type in TYPE_MAPPING) {
        return TYPE_MAPPING[data_type];
    }
    return SqlType.Other;
}


function tokenize(str: string) {
    let text = str.replace(/(?:\r\n|\r|\n)/g, ' ');
    return text.split(' ').filter((token) => token != '');
}


function makeSortedCompletion(val: string, idx: number) {
    let item = new vscode.CompletionItem(val);
    item.sortText = intToSortText(idx);
    return item;
}

function mergeSuggestions(sorted: string[], all: string[]) {
    let results = sorted.filter((column) => includes(all, column));
    all.forEach((column) => {
        if (!includes(results, column)) {
            results.push(column);
        }
    });
    return results;
}


class TableInfo {
    public forward: any;
    public reverse: any;

    constructor() {
        this.forward = {};
        this.reverse = {};
    }

    public addColumn(row: any) {
        if (!(row.table_schema in this.forward)) {
            this.forward[row.table_schema] = {};
        }
        if (!(row.table_name in this.forward[row.table_schema])) {
            this.forward[row.table_schema][row.table_name] = {};
        }
        this.forward[row.table_schema][row.table_name][row.column_name] = {
            data_type: row.data_type,
        };

        if (!(row.column_name in this.reverse)) {
            this.reverse[row.column_name] = {};
        }
        if (!(row.table_name in this.reverse[row.column_name])) {
            this.reverse[row.column_name][row.table_name] = [];
        }
        this.reverse[row.column_name][row.table_name].push(row.table_schema);
    }

    enumerateTables(): Table[] {
        let ret = [];
        for (var schema in this.forward) {
            for (var table in this.forward[schema]) {
                ret.push(new Table(schema, table));
            }
        }
        return ret;
    }

    public lookupTableFields(table: Table): Field[] {
        return Object.keys(this.forward[table.schema][table.table]).map((c) => {
            return new Field(table.schema, table.table, c);
        });
    }

    public lookupField(field: Field) {
        return this.forward[field.schema][field.table][field.attribute];
    }

    public findPossibleTables(field: Field): Table[] {
        if (field.schema !== undefined && field.table !== undefined) {
            return [new Table(field.schema, field.table)];
        }
        if (!(field.attribute in this.reverse)) { return []; }

        let possible = [];
        let table: string;
        for (table in this.reverse[field.attribute]) {
            if (field.table !== undefined && field.table !== table) {
                continue;
            }

            this.reverse[field.attribute][table].forEach((schema: string) => {
                let t = new Table(schema, table);
                possible.push(t);
            });
        }
        return possible;
    };
}


class SqlCompletionItemProvider implements vscode.CompletionItemProvider {
    private db;
    private dbName;
    private tableInfo: Promise<TableInfo>;
    private stats: Stats;

    // TODO split logger to stats and logger
    constructor(db, dbName, stats: Stats) {
        this.db = db;
        this.dbName = dbName;
        this.tableInfo = this.getTableInfo();
        this.stats = stats;
    }

    // db could have been instantiated via connection info string or object,
    // and does not seem to expose this info directly
    private getDbName() {
        return this.db.one("SELECT current_database();").then((ret) => {
            return ret.current_database;
        });
    }

    private getTableInfo() {
        let tableInfo = new TableInfo();
        return this.db.any(
            "SELECT table_schema, table_name, column_name, data_type " +
            "FROM information_schema.columns " +
            "WHERE table_catalog = ${dbName} " +
            "AND table_schema != 'pg_catalog' " +
            "AND table_schema != 'information_schema';",
            { dbName: this.dbName }
        ).then((rows) => {
            rows.forEach((row) => {
                tableInfo.addColumn(row);
            });
            return tableInfo;
        }).catch((err) => {
            console.log(`Error retreiving table info: ${err}`);
        });
    }

    private completeFieldNames(tables: Table[]) {
        console.log('Completing field names');
        return this.tableInfo.then((tableInfo) => {
            let suggestions = [];
            tables.forEach((source) => {
                return tableInfo.lookupTableFields(source).map((col) => {
                    suggestions.push(new vscode.CompletionItem(col.toString()));
                });
            });
            return suggestions;
        });
    }

    public provideCompletionItems(
        document: vscode.TextDocument,
        position: vscode.Position,
        token: vscode.CancellationToken
    ): Thenable<vscode.CompletionItem[]> {
        let parser = new LooseSqlParser();
        // Get cursor offset and tokenize up to cursor
        let offset = document.offsetAt(position);
        let text = document.getText();
        const tokensAtCursor = tokenize(text.slice(0,offset));
        if (tokensAtCursor.length == 0) { return Promise.resolve([])};
        // Save parser state at cursor
        parser.feedTokens(tokensAtCursor);
        let cursorState = parser.state;
        let cursorWhere = parser.currentWhere;
        let cursorOn = parser.currentOn;
        let field;

        // Continue parsing to end
        const tokens = tokenize(text.slice(offset));
        parser.feedTokens(tokens);
        // TODO: handle autocompleting dots

        // TODO handle commas, as when completing after first item
        // (e.g. value after the value right after FROM, SELECT, etc.)
        let stats = this.stats;
        switch (cursorState) {
        case ParserState.From:
            console.log('Completing table names');
            return this.tableInfo.then((tableInfo) => {
                let selectFields = parser.query.select
                    .filter((f) => f != '*')
                    .filter((f) => (!(f.toUpperCase().startsWith('COUNT('))))
                    .map((f) => new Field(f));
                let whereFields = parser.query.where.map((cond) => cond.first);
                let mentionedFields = merge(selectFields, whereFields, field_equals);

                let reverse = flatten(mentionedFields.map((f) => {
                    return tableInfo.findPossibleTables(f);
                })).map((t) => t.toString());
                let historical = stats.getTables();
                let sorted = merge(reverse, historical, table_equals);

                let all = tableInfo
                    .enumerateTables()
                    .filter((t) => {
                        return !includes(parser.query.from, t, table_equals);
                    })
                    .map((t) => { return t.toString(); });

                return mergeSuggestions(sorted, all)
                    .map(makeSortedCompletion);
            });
        case ParserState.Select:
            console.log('Completing field names');
            return this.tableInfo.then((tableInfo) => {
                let suggestions = [];
                let sortedSuggestions = [];
                parser.query.from.forEach((source) => {
                    tableInfo.lookupTableFields(source).forEach((col) => {
                        suggestions.push(col.toString());
                    })

                    let sorted = stats.getColumns(source.schema, source.table);
                    sorted.forEach((col) => {
                        sortedSuggestions.push(col);
                    })
                });

                // Always suggest *
                const star = new vscode.CompletionItem('*');
                star.sortText = "-1";

                return mergeSuggestions(sortedSuggestions, suggestions)
                    .map(makeSortedCompletion)
                    .concat([star]);
            });
        case ParserState.Where_first:
        case ParserState.On_first:
            return this.completeFieldNames(parser.query.from);
        case ParserState.Where_op:
        case ParserState.On_op:
            field = (cursorWhere || cursorOn).first;
            return this.tableInfo.then((tableInfo) => {
                const info = tableInfo.lookupField(field);
                const fieldtype = toSqlType(info.data_type);
                let operators = [ "=", "<>", "!=" ];
                switch (fieldtype) {
                case SqlType.Numeric:
                case SqlType.Temporal:
                    operators.push(...["<", "<=", ">", ">="]);
                    break;
                case SqlType.String:
                case SqlType.Other:
                default:
                    break;
                }
                return operators.map(makeSortedCompletion);
            });
        case ParserState.Where_second:
            field = cursorWhere.first;

            // TODO: guess tableName, schemaName from currentWhere.first
            // if not given as part of a FROM clause

            if (parser.query.where.length === 0) {
                console.log('Autocompleting values via pg_stats');
                // No other conditions, use PG statistics
                // for faster and more accurate suggestions

                // TODO: handle multiple FROM tables (e.g. joins)
                let schemaName = parser.query.from[0].schema;
                return this.db.oneOrNone(
                    "SELECT most_common_vals " +
                    "FROM pg_stats " +
                    "WHERE tablename=${tableName} " +
                    (schemaName !== "" ? "AND schemaname=${schemaName} ": "") +
                    "AND attname=${field};",
                    {
                        tableName: parser.query.from[0].table,
                        schemaName: schemaName,
                        field: field.attribute,
                    }
                ).then((common_vals) => {
                    const vals = parsePgSeq(common_vals.most_common_vals);
                    return vals.map(makeSortedCompletion);
                }).catch((err) => {
                    // Could not get common vals,
                    // most likely due to passing an invalid name,
                    // so just don't provide any completions
                    return Promise.resolve([]);
                });
            } else {
                console.log('Autocompleting values via sampling');
                // Fall back to sampling

                const sampleQuery = parser.query.samplingQuery(field);
                console.log(sampleQuery);
                return this.db.any(sampleQuery).then((rows) => {
                    // TOOD: handle all counts being 1 (e.g. no mode!)
                    // TODO: also compute mean, median, quartiles?
                    return rows.map((row) => {
                        let item = new vscode.CompletionItem(
                            row[field.attribute]
                        );
                        let count = -Number.parseInt(row.count);
                        item.sortText = intToSortText(count);
                        return item;
                    });
                });
            }
        case ParserState.On_second:
            console.log("Autocompleting join suggestions")
            let first = cursorOn.first;
            let tables = parser.query.from.filter((table) => {
                return !table_equals(first.toTable(), table);
            });
            return this.tableInfo.then((tableInfo) => {
                const fieldtype = tableInfo.forward[first.schema][first.table][first.attribute].data_type;
                let suggestions = [];
                tables.forEach((table) => {
                    let cols = tableInfo.forward[table.schema][table.table];
                    Object.keys(cols).forEach((column) => {
                        if (cols[column].data_type === fieldtype) {
                            let temp = new Field(table.schema, table.table, column);
                            suggestions.push(temp.toString());
                        }
                    });
                });
                return suggestions.map((val) => new vscode.CompletionItem(val));
            });
            
        default:
            break;
        }

        return Promise.resolve([]);

        // TODO: use `token`
    }
}

export { SqlCompletionItemProvider };
