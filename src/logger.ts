'use strict'

import * as Winston from 'winston';
import * as vscode from 'vscode';
import { LooseSqlParser, Table, Condition, SqlQuery } from './sql_parse';


const fs = require('fs');
const readline = require('readline');
const path = require('path');


function computeFrecency(freq : number, recency : number) {
    let now = Date.now();
    let factor = 1- (.1 * (now - recency)/ 3600000); // Decreases by .1 every hour
    return Math.max((factor/100), .2) * freq;
}

const parser = new LooseSqlParser();

class Stats {
    private stats: any;
    private dbName: string;

    constructor(dbName: string) {
        this.dbName = dbName;
        this.stats = {};
    }

    public update(query: SqlQuery, stamp: number) {
        // update columns counts
        let tables = query.from;
        let columns = query.select;
        tables.forEach((table) => {
            let fullName = table.toString();
            if (!(fullName in this.stats)) {
                this.stats[fullName] = {};
            }
            columns.forEach((column) => {
                if (!(column in this.stats[fullName])) {
                    this.stats[fullName][column] = {
                        freq : 0,
                        stamp : stamp
                    }
                }
                this.stats[fullName][column].freq += 1;
                let ts = this.stats[fullName][column].stamp;
                this.stats[fullName][column].stamp = Math.max(ts, stamp);

            }, this);
        }, this);
    }

    public getColumns(schema: string, table: string) : string[] {
        let tableName = `${schema}.${table}`;
        if (!(tableName in this.stats)) {
            return [];
        }
        const tbl = this.stats[tableName];
        let columns = Object.keys(tbl);

        columns.sort((a, b) => {
            let aScore = computeFrecency(tbl[a].freq, tbl[a].stamp);
            let bScore = computeFrecency(tbl[b].freq, tbl[b].stamp);
            return bScore - aScore;
        })
        return columns;
    }

    public getTables() : string[] {
        let tables = [];
        let stats = this.stats;
        Object.keys(stats).forEach((tbl) => {

           let values = Object.keys(stats[tbl]).map((col) => {
               let columnStat = stats[tbl][col];
               return computeFrecency(columnStat.freq, columnStat.stamp);
           });

           let total = values.reduce((a, b) => a + b,  0);
           tables.push({name : tbl, score: total});
        });

        tables.sort((a, b) => {
            return b.score - a.score;
        });

        return tables.map((tbl) => tbl.name);
    }
}

class Logger {
    private logger;
    private logfile: string;
    private stats: Stats;

    constructor(dbName: string, stats: Stats) {
        this.logfile = path.join(vscode.workspace.rootPath, `${dbName}_queries.log`);
        this.logger = new Winston.Logger({
            transports : [
                (new Winston.transports.File({
                    filename : this.logfile,
                    level : 'info'
                })),
            ],
        });
        this.stats = stats;
        this.tabulate();
    }

    public log(query : string) : void {
        this.logger.info(query);
        this.consume(query, Date.now());
    }

    public tabulate(): void {
        fs.open(this.logfile, 'r', (err, fd) => {
            if (err) {
                if (err.code === "ENOENT") {
                    console.log(`Couldn't find ${this.logfile}, skipping tabulation`);
                    return;
                } else {
                    throw err;
                }
            }
            console.error('Ingesting previous query log');

            const rl = readline.createInterface({
                input: fs.createReadStream(null, {fd: fd,}),
            })
            let that = this;
            rl.on('line', (line) => {
                // TODO: Parse query and calculate stuff
                let query = JSON.parse(line);
                that.consume(query.message, Date.parse(query.timestamp));
            });
        });
    }

    private consume(query: string, stamp: number): void {
        let text = query.replace(/(?:\r\n|\r|\n)/g, ' ');
        text = text.replace(';', '');
        let tokens = text.split(' ').filter((token) => token != '');
        parser.feedTokens(tokens);
        this.stats.update(parser.query, stamp);
        parser.reset();
    }
}


export { Logger, Stats };
