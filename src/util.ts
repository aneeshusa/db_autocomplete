'use strict';

// Utility functions

// Polyfill for ES7 Array.prototype.includes
export function includes(array: any[], value: any, comparator?): boolean {
    if (comparator === undefined) {
        return array.indexOf(value) !== -1;
    }
    for (let i = 0; i < array.length; i++) {
        if (comparator(array[i], value) === true) {
            return true;
        }
    }
    return false;
}

// Flatten an Array of Arrays into a single array.
// Only goes one level deep (meant to be used with map for flatMap).
export function flatten(arrays: any[][]): any[] {
    return arrays.reduce((a, b) => a.concat(b), []);
}


export function merge(array1: any[], array2: any[], comparator?): any[] {
    if (comparator === undefined) {
        comparator = ((a, b) => a === b);
    }

    let ret = [];
    // Two loops to avoid concat which allocates a large combined array,
    // which will just be GC'ed immediately anyways
    array1.forEach((val) => {
        if (!(includes(ret, val, comparator))) {
            ret.push(val);
        }
    });
    array2.forEach((val) => {
        if (!(includes(ret, val, comparator))) {
            ret.push(val);
        }
    });
    return ret;
}

// Parses a Postgres sequence into an array,
// which is originally obtained as a string
// since pg-promise does not parse it.
export function parsePgSeq(seq: string): string[] {
    console.assert(seq[0] === "{");
    console.assert(seq[seq.length - 1] === "}");
    return seq.substr(1, seq.length - 2).split(',');
}

// Removes a suffix on a string if the suffix exists,
// otherwise returns the original string
export function stripSuffix(suffix: string, slug: string): string {
    if (!(slug.endsWith(suffix))) { return slug; }
    return slug.substr(0, slug.length - suffix.length);
}

// Never again left-pad.
export function padLeft(length: number, pad: string, slug: string): string {
    const paddingValue = Array(length + 1).join(pad);
    return String(paddingValue + slug).slice(-length);
};

// CompletionItems are sorted via string comparison on their
// .sortText properties, so left-pad integers with zeros to
// ensure proper sorting
export function intToSortText(num: number): string {
    // Hopefully 20 is enough for everybody
    return padLeft(20, "0", num.toString());
}
