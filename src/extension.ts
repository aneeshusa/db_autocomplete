'use strict';

import * as vscode from 'vscode';

// Sorted by library name
import Promise = require('bluebird');
import Table = require('easy-table');
import * as pgPromise from 'pg-promise';

import { SqlCompletionItemProvider } from './sql_completion';
import { Logger, Stats } from './logger';


const CONNINFO_ENV_VAR: string = "DB_AUTOCOMPLETE_CONNINFO";
const RUN_QUERY_COMMAND: string = 'db_autocomplete.runQuery';

// Workaround for https://stackoverflow.com/questions/39063281
let pgp = pgPromise({ promiseLib: Promise });
let db = null;
if (CONNINFO_ENV_VAR in process.env) {
    let conninfo = process.env[CONNINFO_ENV_VAR];
    console.log(`Using ${conninfo} as connection info`);
    db = pgp(conninfo);
}

let resultsPane: vscode.OutputChannel = null;
let showResult = (message) => {
    if (resultsPane === null) {
        resultsPane = vscode.window.createOutputChannel(
            'SQL Query Results'
        );
    };
    resultsPane.clear();
    resultsPane.append(message);
    resultsPane.show(true);
};

let logger = null;
let stats = null;
export function activate(context: vscode.ExtensionContext) {
    console.log('db_autocomplete has been activated');
    return Promise.resolve().then(() => {
        if (db === null) {
            console.log('No DB connection info found');
            return null;
        } else {
            // `db` does not expose this info directly,
            // and this is more robust than parsing the connection string
            // ourselves, and also gives us a chance to check that the
            // the connection info is good, i.e. gives access
            return db.one("SELECT current_database();").then((row) => {
                return row.current_database;
            });
        }
    }).then((dbName) => {
        if (dbName !== null) {
            stats = new Stats(dbName)
            logger = new Logger(dbName, stats);
            const completer = vscode.languages.registerCompletionItemProvider(
                'sql', new SqlCompletionItemProvider(db, dbName, stats), ' ', '\n'
            );
            context.subscriptions.push(completer);
        }

        let rq = vscode.commands.registerCommand(RUN_QUERY_COMMAND, () => {
            if (db === null) {
                vscode.window.showInformationMessage(
                    `No DB connection info, set ${CONNINFO_ENV_VAR} env var`
                );
            } else if (dbName === null) {
                vscode.window.showInformationMessage(
                    `Bad DB connection info, check ${CONNINFO_ENV_VAR} env var`
                );
            } else {
                let query = vscode.window.activeTextEditor.document.getText();
                console.log(`Running query: ${query}`);
                db.any(query).then((ret) => {
                    console.log(`Query result: ${ret}`);
                    logger.log(query);
                    // Prefix results with result count.
                    showResult(`${ret.length} results.\n${Table.print(ret)}`);
                }).catch((err) => {
                    console.log(`Query error: ${err}`);
                    showResult(err.toString());
                });
            }
        });
        context.subscriptions.push(rq);
    }).catch((err) => {
        console.log(`error: db_autocomplete encountered an error: ${err}`);
    });
}

export function deactivate() {
    console.log('db_autocomplete has been deactivated');
}
